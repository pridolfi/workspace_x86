/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    printf("Hola mundo! Presione 1 o 2 para simular sensor. ESC para salir.\n");

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            hw_AbrirBarrera();
        }

        if (input == SENSOR_2) {
            hw_CerrarBarrera();
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
